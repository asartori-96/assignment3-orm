package assignment3.orm.dao;

import java.util.ArrayList;

import assignment3.orm.entities.Professor;

public class ProfessorDAO {
	public DAO dao;

	public ProfessorDAO() {
	}

	public void persist(Professor professor) {
		dao.getCurrentSession().save(professor);
	}

	public void update(Professor professor) {
		dao.getCurrentSession().update(professor);
	}

	public Professor findById(String idCollaboratore) {
		Professor professor = (Professor) dao.getCurrentSession().get(Professor.class, idCollaboratore);
		return professor;
	}

	public void delete(Professor professor) {
		dao.getCurrentSession().delete(professor);
	}

	@SuppressWarnings("unchecked")
	public ArrayList<Professor> findAll() {
		ArrayList<Professor> professors = (ArrayList<Professor>) dao.getCurrentSession()
				.createQuery("from Professor").list();
		return professors;
	}

	public void deleteAll() {
		ArrayList<Professor> professorsList = findAll();
		for (Professor professor : professorsList) {
			delete(professor);
		}
	}

}
