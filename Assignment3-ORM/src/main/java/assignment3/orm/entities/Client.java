package assignment3.orm.entities;

import java.util.ArrayList;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "cliente")
public class Client {

	@Id
	@Column(name = "partita_iva")
	private String partitaIva;

	@Column(name = "denominazione_sociale")
	private String denominazioneSociale;

	@Column(name = "settore")
	private String settore;

	// default constructor
	public Client() {

	}

	// constructor
	public Client(String partitaIva, String denominazioneSociale, String settore,
			ArrayList<Project> commissionedProjects) {
		super();
		this.partitaIva = partitaIva;
		this.denominazioneSociale = denominazioneSociale;
		this.settore = settore;
	}

	// getter and setter methods
	public String getPartitaIva() {
		return partitaIva;
	}

	public void setPartitaIva(String partitaIva) {
		this.partitaIva = partitaIva;
	}

	public String getDenominazioneSociale() {
		return denominazioneSociale;
	}

	public void setDenominazioneSociale(String denominazioneSociale) {
		this.denominazioneSociale = denominazioneSociale;
	}

	public String getSettore() {
		return settore;
	}

	public void setSettore(String settore) {
		this.settore = settore;
	}

}
