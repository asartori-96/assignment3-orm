package assignment3.orm.dao;

import java.util.ArrayList;

import assignment3.orm.entities.Client;
//import assignment3.orm.entities.Project;

public class ClientDAO {

	public DAO dao = new DAO();
	
	public ClientDAO() {
	}

	public void persist(Client client) {
		dao.getCurrentSession().save(client);
	}

	public void update(Client client) {
		dao.getCurrentSession().update(client);
	}

	public Client findById(String partitaIva) {
		Client client = (Client) dao.getCurrentSession().get(Client.class, partitaIva);
		return client; 
	}

	public void delete(Client client) {
		dao.getCurrentSession().delete(client);
	}

	@SuppressWarnings("unchecked")
	public ArrayList<Client> findAll() {
		ArrayList<Client> clients = (ArrayList<Client>) dao.getCurrentSession().createQuery("from Client").list();
		return clients;
	}

	public void deleteAll() {
		ArrayList<Client> clientsList = findAll();
		for (Client client : clientsList) {
			delete(client);
		}
	}
}