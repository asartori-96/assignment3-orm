# Assignment 3 - ORM
## Team di sviluppo
Questo assignment è stato svolto in un gruppo composto da due studenti
* **Ruggieri Andrea** [@andreok96] matricola **806808**
* **Sartori Andrea** [@asartori-96] matricola **807473**

## Overview
Considerato un dominio, lo scopo di questo progetto è implementarne le entità e le associazioni in un linugaggio *object-oriented* e di persistere 
gli oggetti istanziati in un database, mantenendo i vincoli di itegrità referenziale definiti. 

## Struttura della repository
La repository si compone di 4 branch distinti
* `master` su cui verrà effettuato il *merge* dei restanti tre branch a fine progetto, ossia entro la seconda consegna.
* `develop` che contiene tutti i codici sorgenti, le librerie e gli script SQL necessari per effettuare la *build* dell'applicazione sviluppata
* `artifacts` che contiene tutti gli artefatti creati per pianificare ed illustrare il lavoro svolto: diagramma E.R., diagramma delle classi...
* `report` che contiene il progetto Latex realizzato per redarre la relazione consegnata tramite la piattaforma Moodle

## Setup del progetto
Per facilitare l'installazione del database e la relativa configurazione, si ha deciso di dockerizzare quest'ultimo.
L'intero progetto è stato sviluppato su sistema operativo Windows, pertanto, non abbiamo testato il funzionamento del sistema su altri
sistemi operativi.
### Programmi richiesti
Al fine di installare correttamente il progetto, è necessario avere installati, sulla propria macchina, i seguenti programmi
* **Eclipse IDE** versione utilizzata per lo sviluppo: Oxygen [Download link: <https://www.eclipse.org/oxygen/>]
* **VirtualBox** necessario per il programma successivo [Download link: <https://www.virtualbox.org/>]
* **Docker Toolbox** l'installazione di tale programma si è resa necessaria durante la fase di sviluppo, poiché le macchine utilizzate montano
  Windows 10 Home Edition che non fornisce il servizio di virtualizzazione *Hyper-V* nativamente. Pertanto, tale programma, ci ha permesso di
  implementare la virtualizzazione tramite un *Virtual Machine* VirtualBox. [Download link: <https://docs.docker.com/toolbox/toolbox_install_windows/>]
  Se si ha la possibilità di utilizzare Docker senza Virtual Machine, considerare bene [questa Sezione](###-Docker-con-Hyper\-V) 
* **Git** necessario per effettuare il `clone` del progetto

### Importazione del progetto
Per importare il progetto è necessario clonare il progetto da questa repository. Se si usa Git da linea di comando, usare il comando `git clone`.
Alternativamente è possibile effettuare l'operazione da un qualsiasi client git installato oppure direttamente dal sito di GitLab.
Una volta clonata la repository in locale, da Eclipse effettuare l'importazione del progetto Maven, contenuto nella cartella **Assignment3-ORM**.
* Dalla barra di navigazione `Home > Import...`
* Dalla finestra che compare `Maven > Existing Maven Projects`

Il progetto compare nella perspective *Package Explorer*.

### Start del container del database
**Nota.** La porta di comunicazione del database è la `3306`.

Per far partire il container contenente il database MariaDB, è necessario seguire i seguenti passi
* Da command line, recarsi nella root del progetto, dove è localizzato il file `docker-compose.yml`
* Eseguire `docker-compose up -d`

**Nota.** Sono stati osservati, su alcune macchine, tempi lunghi di attesa prima di poter usare effettivamente il container. Per avere sotto controllo lo stato
di avvio, al posto del comando sopra citato, eseguire `docker-compose up --build` e attendere che il processo sia terminato.

#### Docker con virtualizzazione nativa
Per l'installazione del progetto è stato consigliato l'utilizzo della virtualizzazione tramite Virtual Machine VirtualBox, dal momento che Hibernate è stato
configurato per comunicare con il database all'indirizzo IP della macchina virtuale (`192.168.99.100`). Se si ha la possibilità e si vuole usare Docker,
con la virtualizzazione effettuata dal sistema operativo (e.g. tramite Hyper-V per Windows) è necessario modificare nel progetto il file `hibernate.cfg.xml`,
il valore della proprietà `hibernate.connection.url`, sostituendo l'IP presente con `127.0.0.1`.