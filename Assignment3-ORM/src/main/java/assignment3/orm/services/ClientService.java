package assignment3.orm.services;

import java.util.ArrayList;

import assignment3.orm.dao.ClientDAO;
import assignment3.orm.entities.Client;

public class ClientService {
	
	private static ClientDAO clientDao;
	
	public ClientService() {
		clientDao = new ClientDAO();
	}
	
	public void persist(Client entity) {
		clientDao.dao.openCurrentSessionwithTransaction();
		clientDao.persist(entity);
		clientDao.dao.closeCurrentSessionwithTransaction();
	}

	public void update(Client entity) {
		clientDao.dao.openCurrentSessionwithTransaction();
		clientDao.update(entity);
		clientDao.dao.closeCurrentSessionwithTransaction();
	}

	public Client findById(String id) {
		clientDao.dao.openCurrentSession();
		Client Client = clientDao.findById(id);
		clientDao.dao.closeCurrentSession();
		return Client;
	}

	public void delete(String id) {
		clientDao.dao.openCurrentSessionwithTransaction();
		Client Client = clientDao.findById(id);
		clientDao.delete(Client);
		clientDao.dao.closeCurrentSessionwithTransaction();
	}

	public ArrayList<Client> findAll() {
		clientDao.dao.openCurrentSession();
		ArrayList<Client> clients = clientDao.findAll();
		clientDao.dao.closeCurrentSession();
		return clients;
	}

	public void deleteAll() {
		clientDao.dao.openCurrentSessionwithTransaction();
		clientDao.deleteAll();
		clientDao.dao.closeCurrentSessionwithTransaction();
	}

	public ClientDAO clientDao() {
		return clientDao;
	}
	
}
