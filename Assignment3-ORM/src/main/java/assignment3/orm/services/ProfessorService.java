package assignment3.orm.services;

import java.util.ArrayList;

import assignment3.orm.dao.ProfessorDAO;
import assignment3.orm.entities.Professor;

public class ProfessorService {
	
	private static ProfessorDAO professorDao;
	
	public ProfessorService() {
		professorDao = new ProfessorDAO();
	}
	
	public void persist(Professor entity) {
		professorDao.dao.openCurrentSessionwithTransaction();
		professorDao.persist(entity);
		professorDao.dao.closeCurrentSessionwithTransaction();
	}

	public void update(Professor entity) {
		professorDao.dao.openCurrentSessionwithTransaction();
		professorDao.update(entity);
		professorDao.dao.closeCurrentSessionwithTransaction();
	}

	public Professor findById(String id) {
		professorDao.dao.openCurrentSession();
		Professor Professor = professorDao.findById(id);
		professorDao.dao.closeCurrentSession();
		return Professor;
	}

	public void delete(String id) {
		professorDao.dao.openCurrentSessionwithTransaction();
		Professor Professor = professorDao.findById(id);
		professorDao.delete(Professor);
		professorDao.dao.closeCurrentSessionwithTransaction();
	}

	public ArrayList<Professor> findAll() {
		professorDao.dao.openCurrentSession();
		ArrayList<Professor> clients = professorDao.findAll();
		professorDao.dao.closeCurrentSession();
		return clients;
	}

	public void deleteAll() {
		professorDao.dao.openCurrentSessionwithTransaction();
		professorDao.deleteAll();
		professorDao.dao.closeCurrentSessionwithTransaction();
	}

	public ProfessorDAO professorDao() {
		return professorDao;
	}
	
}
