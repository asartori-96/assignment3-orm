package assignment3.orm.dao;

import java.util.ArrayList;


import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

import assignment3.orm.entities.Collaborator;

public class CollaboratorDAO {

	public DAO dao = new DAO();

	public CollaboratorDAO() {

	}

	public void persist(Collaborator collaborator) {
		dao.getCurrentSession().save(collaborator);
	}

	public void update(Collaborator collaborator) {
		dao.getCurrentSession().update(collaborator);
	}

	public Collaborator findById(String idCollaboratore) {
		Collaborator collaborator = (Collaborator) dao.getCurrentSession().get(Collaborator.class, idCollaboratore);
		return collaborator;
	}

	public void delete(Collaborator collaborator) {
		dao.getCurrentSession().delete(collaborator);
	}

	@SuppressWarnings("unchecked")
	public ArrayList<Collaborator> findAll() {
		ArrayList<Collaborator> collaborators = (ArrayList<Collaborator>) dao.getCurrentSession()
				.createQuery("from Collaborator").list();
		return collaborators;
	}

	public void deleteAll() {
		ArrayList<Collaborator> collaboratorsList = findAll();
		for (Collaborator collaborator : collaboratorsList) {
			delete(collaborator);
		}
	}
}
