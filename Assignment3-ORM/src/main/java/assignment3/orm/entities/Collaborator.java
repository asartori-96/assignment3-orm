package assignment3.orm.entities;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;

// Class to relational libraries
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Id;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Table;

// Inheritance libraries
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinTable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@Table(name = "collaboratore")
@DiscriminatorColumn(name = "ruolo", discriminatorType = DiscriminatorType.STRING)
public class Collaborator {

	// instance variables
	@Id
	@Column(name = "id_collaboratore")
	private String idCollaboratore;

	@Column(name = "nome")
	private String nome;

	@Column(name = "cognome")
	private String cognome;

	@ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinTable(name = "assegnamento", joinColumns = { @JoinColumn(name = "id_collaboratore") }, inverseJoinColumns = {
			@JoinColumn(name = "codice_progetto") })
	private Set<Project> projects = new HashSet<Project>();

	@ManyToMany(cascade = { CascadeType.ALL })
	@JoinTable(name = "Rapporto", joinColumns = { @JoinColumn(name = "collaborator_id") }, inverseJoinColumns = {
			@JoinColumn(name = "colleague_id") })
	private Set<Collaborator> colleagues = new HashSet<Collaborator>();

	@ManyToMany(mappedBy = "colleagues")
	private Set<Collaborator> team = new HashSet<Collaborator>();

	// default constructor
	public Collaborator() {
	}

	// constructor
	public Collaborator(String idCollaboratore, String codiceFiscale, String nome, String cognome) {
		this.idCollaboratore = idCollaboratore;
		this.nome = nome;
		this.cognome = cognome;
	}

	// getter and setter methods
	public String getIdCollaboratore() {
		return idCollaboratore;
	}

	public void setIdCollaboratore(String idCollaboratore) {
		this.idCollaboratore = idCollaboratore;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCognome() {
		return cognome;
	}

	public void setCognome(String cognome) {
		this.cognome = cognome;
	}

	public void setProjects(Set<Project> project) {
		this.projects.addAll(project);
	}

	public Set<Project> getProject() {
		return this.projects;
	}

	public Set<Collaborator> getColleague() {
		return this.colleagues;
	}

	public void setColleague(Set<Collaborator> colleagues) {
		this.colleagues.addAll(colleagues);
	}

}