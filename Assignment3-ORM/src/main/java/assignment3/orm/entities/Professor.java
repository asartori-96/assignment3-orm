package assignment3.orm.entities;

//Class to relational libraries
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.MappedSuperclass;
import javax.persistence.Table;

@Entity
@DiscriminatorValue("Professore")
public class Professor extends Collaborator {

	// instance variables
	@Column(name = "tipo")
	private String tipo;
	
	@Column(name = "dipartimento")
	private String dipartimento;

	// default constructor 
	public Professor() {
		super();
	}

	// constructor
	public Professor(String idCollaboratore, String codiceFiscale, String nome, String cognome, String tipo, 
			String dipartimento) {
		super(idCollaboratore, codiceFiscale, nome, cognome);
		this.tipo = tipo;
		this.dipartimento = dipartimento;
	}

	// getter and setter methods
	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getDipartimento() {
		return dipartimento;
	}

	public void setDipartimento(String dipartimento) {
		this.dipartimento = dipartimento;
	}

}