package assignment3.orm.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

public class DAO {

	private Session sessionObj;

	private Transaction transactionObj;

	public Session openCurrentSession() {
		sessionObj = getSessionFactory().openSession();
		return sessionObj;
	}

	public Session openCurrentSessionwithTransaction() {
		sessionObj = getSessionFactory().openSession();
		transactionObj = sessionObj.beginTransaction();
		return sessionObj;
	}

	public void closeCurrentSession() {
		sessionObj.close();
	}

	public void closeCurrentSessionwithTransaction() {
		transactionObj.commit();
		sessionObj.close();
	}

	private static SessionFactory getSessionFactory() {
		Configuration configObj = new Configuration();
		configObj.configure("hibernate.cfg.xml");
		ServiceRegistry serviceRegistryObj = new StandardServiceRegistryBuilder()
				.applySettings(configObj.getProperties()).build();
		SessionFactory sessionFactoryObj = configObj.buildSessionFactory(serviceRegistryObj);
		return sessionFactoryObj;
	}

	public Session getCurrentSession() {
		return sessionObj;
	}

	public void setCurrentSession(Session sessionObj) {
		this.sessionObj = sessionObj;
	}

	public Transaction getCurrentTransaction() {
		return transactionObj;
	}

	public void setCurrentTransaction(Transaction transactionObj) {
		this.transactionObj = transactionObj;
	}
}
