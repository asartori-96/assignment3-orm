package assignment3.orm.services;

import java.util.ArrayList;

import org.hibernate.Hibernate;

import assignment3.orm.dao.CollaboratorDAO;
import assignment3.orm.entities.Collaborator;

public class CollaboratorService {

	private static CollaboratorDAO collaboratorDao;

	public CollaboratorService() {
		collaboratorDao = new CollaboratorDAO();
	}

	public void persist(Collaborator entity) {
		collaboratorDao.dao.openCurrentSessionwithTransaction();
		collaboratorDao.persist(entity);
		collaboratorDao.dao.closeCurrentSessionwithTransaction();
	}

	public void update(Collaborator entity) {
		collaboratorDao.dao.openCurrentSessionwithTransaction();
		collaboratorDao.update(entity);
		collaboratorDao.dao.closeCurrentSessionwithTransaction();
	}

	public Collaborator findById(String id) {
		collaboratorDao.dao.openCurrentSession();
		Collaborator Collaborator = collaboratorDao.findById(id);
		collaboratorDao.dao.closeCurrentSession();
		return Collaborator;
	}

	public void delete(String id) {
		collaboratorDao.dao.openCurrentSessionwithTransaction();
		Collaborator Collaborator = collaboratorDao.findById(id);
		collaboratorDao.delete(Collaborator);
		collaboratorDao.dao.closeCurrentSessionwithTransaction();
	}

	public ArrayList<Collaborator> findAll() {
		collaboratorDao.dao.openCurrentSession();
		ArrayList<Collaborator> collaborators = collaboratorDao.findAll();
		collaboratorDao.dao.closeCurrentSession();
		return collaborators;
	}

	public void deleteAll() {
		collaboratorDao.dao.openCurrentSessionwithTransaction();
		collaboratorDao.deleteAll();
		collaboratorDao.dao.closeCurrentSessionwithTransaction();
	}

	public CollaboratorDAO collaboratorDao() {
		return collaboratorDao;
	}

}
