package assignment3.orm.test;

import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.junit.BeforeClass;
import org.junit.Test;

import assignment3.orm.dao.CollaboratorDAO;
import assignment3.orm.entities.Client;
import assignment3.orm.entities.Collaborator;
import assignment3.orm.entities.Professor;
import assignment3.orm.entities.Project;
import assignment3.orm.entities.Researcher;
import assignment3.orm.services.ClientService;
import assignment3.orm.services.CollaboratorService;
import assignment3.orm.services.ProfessorService;
import assignment3.orm.services.ProjectService;
import assignment3.orm.services.ResearcherService;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;
//import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TimeZone;

/**
 * Unit test for simple App.
 */
public class AppTest {

	static Collaborator collaborator1;
	static Project project1;
	static Client client1;
	static Researcher researcher1;

	static CollaboratorService collaboratorService;
	static ProjectService projectService;
	static ClientService clientService;
	static ProfessorService professorService;
	static ResearcherService researcherService;

	@BeforeClass
	public static void init() {
		collaboratorService = new CollaboratorService();
		projectService = new ProjectService();
		clientService = new ClientService();
		professorService = new ProfessorService();
		researcherService = new ResearcherService();
	}

	@Test
	public void testInsertUpdateDeleteCollaborator() {

		collaborator1 = new Collaborator();
		collaborator1.setCognome("Test");
		collaborator1.setNome("Prova");
		collaborator1.setIdCollaboratore("C00001");

		collaboratorService.persist(collaborator1);

		Collaborator collaborator2 = new Collaborator();
		collaborator2 = collaboratorService.findById(collaborator1.getIdCollaboratore());
		assertNotNull(collaborator2);
		assertEquals("Test", collaborator2.getCognome());
		assertEquals("Prova", collaborator2.getNome());

		collaborator1.setNome("Mario");
		collaborator1.setCognome("Rossi");

		collaboratorService.update(collaborator1);
		collaborator2 = collaboratorService.findById(collaborator1.getIdCollaboratore());
		assertNotEquals("Test", collaborator2.getCognome());
		assertNotEquals("Prova", collaborator2.getNome());

		collaboratorService.delete(collaborator1.getIdCollaboratore());

	}

	@Test
	public void testFindDeleteAllCollaborator() {

		Collaborator prov1 = new Collaborator("C70001", "test", "prova", "rrrggg43d55d443d");
		Collaborator prov2 = new Collaborator("C70002", "test", "prova", "rrrggg43d55d443d");
		Collaborator prov3 = new Collaborator("C70003", "test", "prova", "rrrggg43d55d443d");
		Collaborator prov4 = new Collaborator("C70004", "test", "prova", "rrrggg43d55d443d");


		collaboratorService.persist(prov1);
		collaboratorService.persist(prov2);
		collaboratorService.persist(prov3);
		collaboratorService.persist(prov4);

		ArrayList<Collaborator> collaboratorList = new ArrayList<Collaborator>();
		collaboratorList.addAll(collaboratorService.findAll());

		assertNotNull(collaboratorList.get(0));
		assertNotNull(collaboratorList.get(3));
		assertEquals(4, collaboratorList.size());
		assertEquals("C70001", collaboratorList.get(0).getIdCollaboratore());
		assertEquals("C70003", collaboratorList.get(2).getIdCollaboratore());
		collaboratorService.deleteAll();
	}

	@Test
	public void testProjectSemplice() {

		client1 = new Client();
		client1.setDenominazioneSociale("ToRo SPA");
		client1.setPartitaIva("99988877701");
		client1.setSettore("Trasporti");

		project1 = new Project();
		project1.setCodiceProgetto("P99994");
		project1.setCliente(client1);

		clientService.persist(client1);
		projectService.persist(project1);

		ArrayList<Project> projects = new ArrayList<Project>();
		projects.addAll(projectService.findAll());

		assertNotNull(projects);
		assertEquals("P99994", projects.get(0).getCodiceProgetto());
		assertNotNull(projects.get(0).getCliente());

		projectService.deleteAll();
		clientService.deleteAll();

	}

	@Test
	public void testGerarchia() {
		Professor p1 = new Professor();

		p1.setCognome("ciao");
		p1.setDipartimento("informatica");
		p1.setIdCollaboratore("P00001");
		p1.setNome("mondo");
		collaboratorService.deleteAll();
		collaboratorService.persist(p1);

		Researcher r1 = new Researcher();
		r1.setCognome("Stradone");
		r1.setLaboratorio("LAB01-14");
		r1.setIdCollaboratore("P00002");
		r1.setNome("Nello");
		collaboratorService.persist(r1);

		ArrayList<Collaborator> collaborators = new ArrayList<Collaborator>();
		collaborators.addAll(collaboratorService.findAll());

		assertEquals(2, collaborators.size());
		collaboratorService.deleteAll();

	}

	@Test
	public void testRapporto() {
		Collaborator collaborator1 = new Collaborator("C99994", "GGGJJJ21H06U828F", "Giovanni", "Gialli");

		Collaborator collega1 = new Collaborator("C99996", "VVVGGG36F26F626T", "Collega", "Simpatico");
		Collaborator collega2 = new Collaborator("C99995", "AAABBB02C02C444C", "Collega", "Cattivo");

		collaboratorService.persist(collega1);
		collaboratorService.persist(collega2);

		Set<Collaborator> colleagues = new HashSet<Collaborator>();
		colleagues.add(collega1);
		colleagues.add(collega2);

		collaborator1.setColleague(colleagues);

		collaboratorService.persist(collaborator1);

		ArrayList<Collaborator> collaborators = new ArrayList<Collaborator>();
		collaborators.addAll(collaboratorService.findAll());

		assertEquals(3, collaborators.size());
		assertNotNull(collaborators.get(0).getColleague());

		collaboratorService.deleteAll();

		// testo corretto funzionamento metodo deleteAll
		collaborators.clear();
		collaborators.addAll(collaboratorService.findAll());
		assertEquals(true, collaborators.isEmpty());
	}

	@Test
	public void testSchemaCompleto() {

		// Creo nuovi collaborators e rapporti
		Professor p1 = new Professor("C00001", "AAABBB85Y52G728Y", "Primo", "Professore", "Associato", "U14");
		Professor p2 = new Professor("C00002", "HHHRRR73T73G738D", "Secondo", "Professore", "Ordinario", "U2");
		Researcher r1 = new Researcher("C00004", "CCCHHD63G33J392J", "Primo", "Ricercatore", "LAB03-02");

		Set<Collaborator> colleghi = new HashSet<Collaborator>();
		colleghi.add(r1);

		p1.setColleague(colleghi);

		// Creo nuovi clienti e progetti
		Client c1 = new Client();
		c1.setDenominazioneSociale("RoMi SpA");
		c1.setPartitaIva("12345678901");
		c1.setSettore("Trasporti");

		Project progetto1 = new Project();
		progetto1.setCliente(c1);
		progetto1.setCodiceProgetto("P00001");
		progetto1.setCollaborators(r1); // NB assegnamento bidirezionale diverso dal test precedente
		progetto1.setCollaborators(p1);


		// Inserimenti nel DB
		collaboratorService.persist(r1);
		collaboratorService.persist(p1);
		collaboratorService.persist(p2);
		clientService.persist(c1);
		projectService.persist(progetto1);

		// Test sugli update
		c1.setDenominazioneSociale("RoFi - FiMi SpA");
		clientService.update(c1);

		p1.setTipo("Ordinario");
		collaboratorService.update(p1);

		// Elimino tutte tuple del db
		collaboratorService.deleteAll();
		projectService.deleteAll();
		clientService.deleteAll();

	}

	@Test
	public void testLazyLoadingNotLoad() {

		// create collaborator
		Collaborator collaborator = new Collaborator("C99979", "LRTNTF54F35C987F", "Mario", "Verdi");

		// create two calendar dates for instatiation a project
		Calendar dataInizio = Calendar.getInstance(TimeZone.getTimeZone("UTC+1"));
		dataInizio.set(2018, 8, 26);

		Calendar dataFine = Calendar.getInstance(TimeZone.getTimeZone("UTC+1"));
		dataInizio.set(2018, 11, 31);

		// create a client for project instantiation
		Client client = new Client();
		client.setDenominazioneSociale("FakeCompany SPA");
		client.setPartitaIva("12345678955");
		client.setSettore("Servizi informatici");

		// create project
		Project project = new Project("P87859", client, dataInizio, dataFine);



		project.setCollaborators(collaborator1);
		collaboratorService.persist(collaborator);
		projectService.persist(project); // Assegno un progetto al collaborator

		Collaborator collaboratorGet = collaboratorService.findById("C99979");

		assertFalse(Hibernate.isInitialized(collaboratorGet.getProject())); // Deve essere falso

		collaboratorService.deleteAll();
		projectService.deleteAll();
		clientService.deleteAll();

	}

	@Test
	public void testAssegnamentoLazyLoading() {

		client1 = new Client();
		client1.setDenominazioneSociale("ToRo SPA");
		client1.setPartitaIva("99988877732");
		client1.setSettore("Trasporti");

		collaborator1 = new Collaborator();
		project1 = new Project();

		project1.setCodiceProgetto("P99991");
		project1.setCliente(client1);

		Set<Project> projects = new HashSet<Project>();
		projects.add(project1);

		collaborator1.setCognome("Rossi");
		collaborator1.setNome("Andrea");
		collaborator1.setIdCollaboratore("C99991");
		collaborator1.setProjects(projects);

		assertEquals(1, collaborator1.getProject().size());

		clientService.persist(client1);
		projectService.persist(project1);
		collaboratorService.persist(collaborator1);

		ArrayList<Collaborator> collaborators = new ArrayList<Collaborator>();
		collaborators.addAll(collaboratorService.findAll());

		assertNotNull(collaborators.get(0).getProject());

		collaboratorService.deleteAll();
		projectService.deleteAll();
		clientService.deleteAll();

	}

	@Test
	public void TestQuery() {
		
		client1 = new Client();
		client1.setDenominazioneSociale("ToRo SPA");
		client1.setPartitaIva("99988877732");
		client1.setSettore("Trasporti");

		collaborator1 = new Collaborator();
		project1 = new Project();

		project1.setCodiceProgetto("P99991");
		project1.setCliente(client1);

		Set<Project> projects = new HashSet<Project>();
		projects.add(project1);

		collaborator1.setCognome("Rossi");
		collaborator1.setNome("Andrea");
		collaborator1.setIdCollaboratore("C99991");
		collaborator1.setProjects(projects);

		clientService.persist(client1);
		projectService.persist(project1);
		collaboratorService.persist(collaborator1);

		// Selezionare il cliente
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		Session session = sessionFactory.getCurrentSession();

		Transaction tx = session.beginTransaction();

		Query query = session
				.createQuery("select c.nome, a.client from Collaborator c " + "LEFT OUTER JOIN c.projects a " + "where c.cognome='Rossi'");
		List<Object[]> list = (List<Object[]>) query.list();

		assertNotNull(list);
		assertEquals(1, list.size());

		// rolling back to save the test data
		tx.rollback();

		// closing hibernate resources
		sessionFactory.close();
		
		collaboratorService.deleteAll();
		projectService.deleteAll();
		clientService.deleteAll();

		System.out.println(list.get(0));
	}
}
