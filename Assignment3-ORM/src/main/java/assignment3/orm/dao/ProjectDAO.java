package assignment3.orm.dao;

import java.util.ArrayList;

import assignment3.orm.entities.Project;

public class ProjectDAO {
	public DAO dao = new DAO();
	
	public ProjectDAO() {
	}

	public void persist(Project project) {
		dao.getCurrentSession().save(project);
	}

	public void update(Project project) {
		dao.getCurrentSession().update(project);
	}

	public Project findById(String partitaIva) {
		Project project = (Project) dao.getCurrentSession().get(Project.class, partitaIva);
		return project; 
	}

	public void delete(Project project) {
		dao.getCurrentSession().delete(project);
	}

	@SuppressWarnings("unchecked")
	public ArrayList<Project> findAll() {
		ArrayList<Project> projects = (ArrayList<Project>) dao.getCurrentSession().createQuery("from Project").list();
		return projects;
	}

	public void deleteAll() {
		ArrayList<Project> projectsList = findAll();
		for (Project project : projectsList) {
			delete(project);
		}
	}
}
