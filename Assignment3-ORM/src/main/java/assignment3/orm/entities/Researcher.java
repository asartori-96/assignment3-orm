package assignment3.orm.entities;

//Class to relational libraries
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@DiscriminatorValue("Ricercatore")
public class Researcher extends Collaborator {

	@Column(name = "laboratorio")
	private String laboratorio;

	// default constructor
	public Researcher() {
		super();
	}

	// constructor
	public Researcher(String idCollaboratore, String codiceFiscale, String nome, String cognome, String laboratorio) {
		super(idCollaboratore, codiceFiscale, nome, cognome);
		this.laboratorio = laboratorio;
	}

	// getter and setter methods
	public String getLaboratorio() {
		return laboratorio;
	}

	public void setLaboratorio(String laboratorio) {
		this.laboratorio = laboratorio;
	}
}