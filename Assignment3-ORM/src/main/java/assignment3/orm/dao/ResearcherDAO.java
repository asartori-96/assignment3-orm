package assignment3.orm.dao;

import java.util.ArrayList;

import assignment3.orm.entities.Researcher;

public class ResearcherDAO {
	public DAO dao;

	public ResearcherDAO() {
	}

	public void persist(Researcher researcher) {
		dao.getCurrentSession().save(researcher);
	}

	public void update(Researcher researcher) {
		dao.getCurrentSession().update(researcher);
	}

	public Researcher findById(String idCollaboratore) {
		Researcher researcher = (Researcher) dao.getCurrentSession().get(Researcher.class, idCollaboratore);
		return researcher;
	}

	public void delete(Researcher researcher) {
		dao.getCurrentSession().delete(researcher);
	}

	@SuppressWarnings("unchecked")
	public ArrayList<Researcher> findAll() {
		ArrayList<Researcher> researchers = (ArrayList<Researcher>) dao.getCurrentSession().createQuery("from Researcher").list();
				

		return researchers;
	}

	public void deleteAll() {
		ArrayList<Researcher> researchersList = findAll();
		for (Researcher researcher : researchersList) {
			delete(researcher);
		}
	}

}
