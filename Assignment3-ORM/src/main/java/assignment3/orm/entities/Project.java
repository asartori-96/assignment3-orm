package assignment3.orm.entities;

import java.util.Calendar;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "Progetto")
public class Project {

	@Id
	@Column(name = "codice_progetto")
	private String codiceProgetto;

	@Temporal(value = TemporalType.DATE)
	@Column(name = "data_inizio")
	Calendar dataInizio;

	@Temporal(value = TemporalType.DATE)
	@Column(name = "data_fine")
	Calendar dataFine;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "partita_iva", nullable = false, updatable = false)
	private Client client;

	@ManyToMany(mappedBy = "projects", fetch = FetchType.LAZY)
	private Set<Collaborator> collaborators = new HashSet<Collaborator>();

	// default constructor
	public Project() {
	}

	// constructor
	public Project(String codiceProgetto, Client cliente, Calendar dataInizio, Calendar dataFine) {
		super();
		this.codiceProgetto = codiceProgetto;
		this.client = cliente;
		this.dataInizio = dataInizio;
		this.dataFine = dataFine;
	}

	// getter and setter methods
	public String getCodiceProgetto() {
		return codiceProgetto;
	}

	public void setCodiceProgetto(String codiceProgetto) {
		this.codiceProgetto = codiceProgetto;
	}

	public Client getCliente() {
		return client;
	}

	public void setCliente(Client cliente) {
		this.client = cliente;
	}

	public Calendar getDataInizio() {
		return dataInizio;
	}

	public void setDataInizio(Calendar dataInizio) {
		this.dataInizio = dataInizio;
	}

	public Calendar getDataFine() {
		return dataFine;
	}

	public void setDataFine(Calendar dataFine) {
		this.dataFine = dataFine;
	}

	public void setCollaborators(Collaborator collaborator) {
		this.collaborators.add(collaborator);
	}

}
