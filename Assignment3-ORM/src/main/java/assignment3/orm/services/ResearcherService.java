package assignment3.orm.services;

import java.util.ArrayList;

import assignment3.orm.dao.ResearcherDAO;
import assignment3.orm.entities.Researcher;

public class ResearcherService {
	
	private static ResearcherDAO researcherDao;
	
	public ResearcherService() {
		researcherDao = new ResearcherDAO();
	}
	
	public void persist(Researcher entity) {
		researcherDao.dao.openCurrentSessionwithTransaction();
		researcherDao.persist(entity);
		researcherDao.dao.closeCurrentSessionwithTransaction();
	}

	public void update(Researcher entity) {
		researcherDao.dao.openCurrentSessionwithTransaction();
		researcherDao.update(entity);
		researcherDao.dao.closeCurrentSessionwithTransaction();
	}

	public Researcher findById(String id) {
		researcherDao.dao.openCurrentSession();
		Researcher Researcher = researcherDao.findById(id);
		researcherDao.dao.closeCurrentSession();
		return Researcher;
	}

	public void delete(String id) {
		researcherDao.dao.openCurrentSessionwithTransaction();
		Researcher Researcher = researcherDao.findById(id);
		researcherDao.delete(Researcher);
		researcherDao.dao.closeCurrentSessionwithTransaction();
	}

	public ArrayList<Researcher> findAll() {
		researcherDao.dao.openCurrentSession();
		ArrayList<Researcher> reseachers = researcherDao.findAll();
		researcherDao.dao.closeCurrentSession();
		return reseachers;
	}

	public void deleteAll() {
		researcherDao.dao.openCurrentSessionwithTransaction();
		researcherDao.deleteAll();
		researcherDao.dao.closeCurrentSessionwithTransaction();
	}

	public ResearcherDAO researcherDao() {
		return researcherDao;
	}
	
}
