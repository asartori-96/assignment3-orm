package assignment3.orm.services;

import java.util.ArrayList;

import assignment3.orm.dao.ProjectDAO;
import assignment3.orm.entities.Project;

public class ProjectService {
	
	private static ProjectDAO projectDao;
	
	public ProjectService() {
		projectDao = new ProjectDAO();
	}
	
	public void persist(Project entity) {
		projectDao.dao.openCurrentSessionwithTransaction();
		projectDao.persist(entity);
		projectDao.dao.closeCurrentSessionwithTransaction();
	}

	public void update(Project entity) {
		projectDao.dao.openCurrentSessionwithTransaction();
		projectDao.update(entity);
		projectDao.dao.closeCurrentSessionwithTransaction();
	}

	public Project findById(String id) {
		projectDao.dao.openCurrentSession();
		Project Project = projectDao.findById(id);
		projectDao.dao.closeCurrentSession();
		return Project;
	}

	public void delete(String id) {
		projectDao.dao.openCurrentSessionwithTransaction();
		Project Project = projectDao.findById(id);
		projectDao.delete(Project);
		projectDao.dao.closeCurrentSessionwithTransaction();
	}

	public ArrayList<Project> findAll() {
		projectDao.dao.openCurrentSession();
		ArrayList<Project> projects = projectDao.findAll();
		projectDao.dao.closeCurrentSession();
		return projects;
	}

	public void deleteAll() {
		projectDao.dao.openCurrentSessionwithTransaction();
		projectDao.deleteAll();
		projectDao.dao.closeCurrentSessionwithTransaction();
	}

	public ProjectDAO projectDao() {
		return projectDao;
	}
	
}
